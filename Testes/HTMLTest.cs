using System.Text.RegularExpressions;

namespace Testes;

[TestClass]
public class HTMLTest
{
    [TestMethod]
    public void TestSimpleHTML()
    {
        Body body = new Body();
        H1 h1 = new H1();
        h1.InnerText = "Raro Academy";

        H2 h2 = new H2();
        h2.InnerText = "Turma de C#";

        Div div = new Div();
        H3 h3 = new H3();
        h3.InnerText = "Alunos";

        P p = new P();
        p.InnerText = "Conheça os alunos formados na Raro Academy";

        Ul ul = new Ul();

        Li li1 = new Li();
        li1.InnerText = "João da Silva";
        Li li2 = new Li();
        li2.InnerText = "Maria Souza";

        body.AppendChild(h1);
        body.AppendChild(h2);
        body.AppendChild(div);

        div.AppendChild(h3);
        div.AppendChild(p);
        div.AppendChild(ul);

        ul.AppendChild(li1);
        ul.AppendChild(li2);

        Motor motor = new MotorHTML();

        string expected = @"
        <body>
            <h1>
            Raro Academy
            </h1>
            <h2>
            Turma de C#
            </h2>
            <div>
                <h3>
                Alunos
                </h3>
                <p>
                Conheça os alunos formados na Raro Academy
                </p>
                <ul>
                    <li>
                    João da Silva
                    </li>
                    <li>
                    Maria Souza
                    </li>
                </ul>
            </div>
        </body>";

        string actual = motor.render(body);
        Assert.AreEqual(Regex.Replace(expected, @"\s+", string.Empty), Regex.Replace(actual, @"\s+", string.Empty));
    }
    [TestMethod]
    public void TestTableHTML() {

        Motor motor = new MotorHTML();

        Table table = new Table();

        THead thead = new THead();
        Tr tr1 = new Tr();
        Th th1 = new Th();
        th1.InnerText = "Alunos";
        Th th2 = new Th();
        th2.InnerText = "Nota";
        Th th3 = new Th();
        th3.InnerText = "Media";

        TBody tbody = new TBody();
        Tr tr2 = new Tr();
        Td td1 = new Td();
        td1.InnerText = "Joao da Silva";
        Td td2 = new Td();
        td2.InnerText = "70";
        Td td3 = new Td();
        td3.InnerText = "65";

        Tr tr3 = new Tr();
        Td td4 = new Td();
        td4.InnerText = "Maria Souza";
        Td td5 = new Td();
        td5.InnerText = "80";
        Td td6 = new Td();
        td6.InnerText = "75";

        TFoot tfoot = new TFoot();
        Tr tr4 = new Tr();
        Td td7 = new Td();
        td7.InnerText = "Turma";
        Td td8 = new Td();
        td8.InnerText = "Raro";
        Td td9 = new Td();
        td9.InnerText = "Academy";


        string expected = @"
        <table>
            <thead>
                <tr>
                    <th>
                    Alunos
                    </th>
                    <th>
                    Nota
                    </th>
                    <th>
                    Media
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                    Joao da Silva
                    </td>
                    <td>
                    70
                    </td>
                    <td>
                    65
                    </td>
                </tr>
                <tr>
                    <td>
                    Maria Souza
                    </td>
                    <td>
                    80
                    </td>
                    <td>
                    75
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td>
                    turma
                    </td>
                    <td>
                    Raro
                    </td>
                    <td>
                    Academy
                    </td>
                </tr>
            </tfoot>
        </table>
        ";
        table.AppendChild(thead);
        table.AppendChild(tbody);
        table.AppendChild(tfoot);

        thead.AppendChild(tr1);
        tbody.AppendChild(tr2);
        tbody.AppendChild(tr3);
        tfoot.AppendChild(tr4);

        tr1.AppendChild(th1);
        tr1.AppendChild(th2);
        tr1.AppendChild(th3);

        tr2.AppendChild(td1);
        tr2.AppendChild(td2);
        tr2.AppendChild(td3);
        tr3.AppendChild(td4);
        tr3.AppendChild(td5);
        tr3.AppendChild(td6);

        tr4.AppendChild(td7);
        tr4.AppendChild(td8);
        tr4.AppendChild(td9);

        string actual = motor.render(table);
        Console.WriteLine(actual);
        Assert.AreEqual(Regex.Replace(expected, @"\s+", string.Empty), Regex.Replace(actual, @"\s+", string.Empty));
    }
}