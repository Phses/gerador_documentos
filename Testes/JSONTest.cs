using System.Text.RegularExpressions;

namespace Testes;

[TestClass]
public class JSONTest
{
    [TestMethod]
    public void TestSimpleJson() {
        Objeto objeto = new Objeto();
        Chave chave1 = new Chave();
        Chave chave2 = new Chave();
        Chave chave3 = new Chave();

        chave1.InnerString = "One Piece";
        chave2.InnerNumber = 1999;
        string[] vector = new string[] {"Luffy", "Zoro", "Nami"};
        chave3.InnerVector = vector;

        Object.AppendChild(chave1);
        Object.AppendChild(chave2);
        Object.AppendChild(chave3);

        Motor motor = new MotorJson();

        string expected = @"{
        ""anime"": ""One Piece"",
        ""Ano"" : 1999
        ""Personagens"" : [""Luffy"",""Zoro"",""Robin"",""Nami""]
        }
        ";

        string actual = motor.render(objeto);
        Assert.AreEqual(Regex.Replace(expected, @"\s+", string.Empty), Regex.Replace(actual, @"\s+", string.Empty));
    }
}