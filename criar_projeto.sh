dotnet new sln -n $1
dotnet new console -n $2
dotnet new mstest -n Testes
dotnet sln add $2/$2.csproj
dotnet sln add Testes/Testes.csproj
dotnet add Testes/Testes.csproj reference $2/$2.csproj
dotnet build
dotnet test
