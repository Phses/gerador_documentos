namespace Editor.Elementos;

public abstract class ElementoHTML : Elemento
{
    private List<Elemento> children = new List<Elemento>();

    public string InnerText { get; set; }

    public void AppendChild(Elemento elemento)
    {
        this.children.Add(elemento);
    }

    public Elemento[] Children()
        => this.children.ToArray();

    public abstract string TagName();
}