namespace Editor.Elementos;

public class Body : ElementoHTML
{
    public override string TagName() => "body";
}