namespace Editor.Elementos;

public interface Elemento
{

    string InnerText { get; set; }

    void AppendChild(Elemento elemento);

    string TagName();

    Elemento[] Children();

}