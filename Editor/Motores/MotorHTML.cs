using Editor.Elementos;

namespace Editor.Motores;

public class MotorHTML : Motor
{
    public string render(Elemento elemento, int nivel = 0)
    {
        string tabs = new String('\t', nivel);

        return $"{tabs}<{elemento.TagName()}>\n{renderChildren(elemento.Children(), nivel)}{text(tabs, elemento)}\n{tabs}</{elemento.TagName()}>";
    }

    private string text(string tabs, Elemento elemento)
    {
        if (elemento.InnerText != null)
        {
            return tabs + elemento.InnerText;
        }
        return "";
    }

    private string renderChildren(Elemento[] children, int nivel = 0)
    {
        return String.Join("\n", children.Select((child) => render(child, nivel + 1)));
    }
}