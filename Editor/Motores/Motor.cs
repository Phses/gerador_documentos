namespace Editor.Motores;
using Editor.Elementos;

public interface Motor
{
    string render(Elemento elemento, int nivel = 0);
}